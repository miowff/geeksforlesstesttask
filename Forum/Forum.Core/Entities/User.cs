﻿using Microsoft.AspNetCore.Identity;

namespace Forum.Core.Entities
{
    public  class User : IdentityUser
    {
        public List<Post> Posts { get; set; } = new List<Post>();
        public List<Topic> Topic { get; set; } = new List<Topic>();
    }
}
