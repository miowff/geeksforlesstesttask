﻿
namespace Forum.Core.Entities
{
    public sealed class Post : BaseEntity
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public Topic Topic { get; set; }
        public User User { get; set; }
    }
}
