﻿
namespace Forum.Core.Entities
{
    public sealed class Topic : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public List<Post> Posts { get; set; } = new List<Post>();
        public User User { get; set; }
    }
}
