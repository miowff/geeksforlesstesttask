﻿using Forum.Core.Entities;
namespace Forum.Data.Interfaces
{
    public interface ITopicsRepository : IGenericRepository<Topic>
    {
        Task<IEnumerable<Topic>> GetAllWithDetailsAsync();
        Task<Topic> GetByIdWithDetailsAsync(Guid id);
    }
}
