﻿using Forum.Core.Entities;

namespace Forum.Data.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(Guid entityId);
        void Delete(Guid entityId);
        void Update(TEntity entity);
        void AddAsync(TEntity entity);
    }
}
