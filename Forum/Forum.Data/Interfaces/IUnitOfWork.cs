﻿using Forum.Core.Entities;
using Microsoft.AspNetCore.Identity;

namespace Forum.Data.Interfaces
{
    public interface IUnitOfWork
    {
        ITopicsRepository TopicsReository { get; }
        IPostsRepository PostsRepository { get; }
        UserManager<User> UserManager { get; }
        RoleManager<IdentityRole> RoleManager { get; }
        Task CompleateAsync();
    }
}
