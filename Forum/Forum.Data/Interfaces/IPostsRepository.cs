﻿using Forum.Core.Entities;
namespace Forum.Data.Interfaces
{
    public interface IPostsRepository : IGenericRepository<Post>
    {
        Task<IEnumerable<Post>> GetAllWithDetailsAsync();
        Task<Post> GetByIdWithDetailsAsync(Guid id);
    }
}
