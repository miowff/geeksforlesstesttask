﻿using Forum.Core.Entities;
using Forum.Data.Interfaces;
using System.Data.Entity;

namespace Forum.Data.Repositories
{
    public sealed class PostsRepository : GenericRepository<Post>, IPostsRepository
    {
        public PostsRepository(ApplicationDbContext dbContext) : base(dbContext) { }

        public async Task<IEnumerable<Post>> GetAllWithDetailsAsync()
        {
           return await dbSet.Include(post => post.User)
                       .Include(post => post.Topic).AsNoTracking().ToListAsync();
        }

        public async Task<Post> GetByIdWithDetailsAsync(Guid id)
        {
            return await dbSet.Include(post => post.User)
                              .Include(post => post.Topic).AsNoTracking().FirstOrDefaultAsync(post => post.Id == id);
        }
    }
}
