﻿using Forum.Core.Entities;
using Forum.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected ApplicationDbContext context { get; set; }
        protected DbSet<TEntity> dbSet;
        protected GenericRepository(ApplicationDbContext dbContext)
        {
            this.context = dbContext;
            this.dbSet = context.Set<TEntity>();
        }
        public async virtual void AddAsync(TEntity entity)
        {
            await dbSet.AddAsync(entity);
        }

        public virtual void Delete(Guid entityId)
        {
            var entityToDelete = dbSet.Find(entityId);
            if (entityToDelete != null)
            {
                dbSet.Remove(entityToDelete);
            }
        }

        public async virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await dbSet.ToListAsync();
        }

        public async virtual Task<TEntity> GetByIdAsync(Guid entityId)
        {
            return await dbSet.FirstOrDefaultAsync(x => x.Id == entityId);
        }

        public virtual void Update(TEntity entity)
        {
            dbSet.Update(entity);
        }
    }
}
