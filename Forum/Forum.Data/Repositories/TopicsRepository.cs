﻿using Forum.Core.Entities;
using Forum.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
namespace Forum.Data.Repositories
{
    public sealed class TopicsRepository : GenericRepository<Topic>, ITopicsRepository
    {
        public TopicsRepository(ApplicationDbContext dbContext) : base(dbContext) { }

        public async Task<IEnumerable<Topic>> GetAllWithDetailsAsync()
        {
            return await dbSet.Include(topic => topic.User)
                              .Include(topic => topic.Posts).AsNoTracking().ToListAsync();
        }

        public async Task<Topic> GetByIdWithDetailsAsync(Guid id)
        {
            return await dbSet.Include(topic => topic.User)
                              .Include(topic => topic.Posts).AsNoTracking().FirstOrDefaultAsync(topic => topic.Id == id);
        }
    }
}
