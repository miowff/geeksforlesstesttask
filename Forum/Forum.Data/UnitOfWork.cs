﻿using Forum.Core.Entities;
using Forum.Data.Interfaces;
using Forum.Data.Repositories;
using Microsoft.AspNetCore.Identity;

namespace Forum.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed = false;
        public ITopicsRepository TopicsReository { get; private set; }

        public IPostsRepository PostsRepository { get; private set; }

        public UserManager<User> UserManager { get; private set; }

        public RoleManager<IdentityRole> RoleManager { get; private set; }
        public UnitOfWork(ApplicationDbContext context, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            this._context = context;
            this.RoleManager = roleManager;
            this.UserManager = userManager;
            this.TopicsReository = new TopicsRepository(context);
            this.PostsRepository = new PostsRepository(context);
        }
        public async Task CompleateAsync()
        {
            await _context.SaveChangesAsync();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
